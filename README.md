# Rodar o Projeto Backend

## Pré requisitos:
* Docker
* Node
* Yarn

## Comandos para rodar o projeto
Antes de rodar o projeto de fato, você precisar clonar e renomear o arquivo `.env.example` para `.env`. Realize as configurações de banco de dados necessárias. Para conveniência, o arquivo `.env.example` está configurado corretamente.

Instale as dependências do projeto, utilizando o comando:
```
yarn
```

### Subir o banco de dados
O banco de dados está configurado em ambiente docker, para subir o mesmo, digite na pasta base do projeto:
```
yarn deploy
```
Rodando esse comando, irá subir o banco de dados no Docker, migrar e gerar as classes do Prisma e também criar os Fornecedores, para conveniência.

### Configuração da porta COM
O projeto depende de uma porta COM em funcionamento, portanto, é necessário configura-lá no arquivo `src/modules/Serial/serial.service.ts`, na linha 16.

Os valores padrão estão:
* path: `COM1`
* baudRate: `9600`

Mude as configurações conforme necessário.

### Subindo o projeto
Para subir o projeto, basta rodar o comando abaixo:
```
yarn start
```

### Rotas expostas
As rotas expostas do projeto são:
* GET /Pesos: Rota que retorna a listagem dos pesos cadastrados.
* WS /socket.io: Rota relacionada ao WebSocket, para atualização automática da listagem.