import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { PesosModule } from './modules/Pesos/pesos.module';
import { SerialModule } from './modules/Serial/serial.module';

@Module({
  imports: [SerialModule, PesosModule],
  providers: [AppService],
})
export class AppModule {}
