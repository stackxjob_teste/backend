import PrismaService from './prisma.service';

const prisma = new PrismaService();

async function main() {
  await prisma.truncate();

  await prisma.fornecedores.createMany({
    data: [
      {
        nome: 'Fornecedor Teste 1',
        descricao: 'Fornecedor 1',
      },
      {
        nome: 'Fornecedor Teste 2',
        descricao: 'Fornecedor 2',
      },
    ],
  });

  console.log('Seeds criadas com sucesso!');
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
