import { Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';

@Injectable()
export default class PrismaService
  extends PrismaClient
  implements OnModuleInit
{
  async onModuleInit() {
    await this.$connect();
  }

  async truncate() {
    // Desativa as restrições de chave estrangeira temporariamente
    await this.$executeRaw`SET foreign_key_checks = 0`;

    // Obtém o nome de todas as tabelas do banco de dados atual
    const tabelas = await this.$queryRaw<
      { TABLE_NAME: string }[]
    >`SELECT table_name FROM information_schema.tables WHERE table_schema = 'balanca';`;

    // Executa a consulta DELETE em cada tabela
    for (const tabela of tabelas) {
      await this.$executeRawUnsafe(`TRUNCATE TABLE ${tabela.TABLE_NAME};`);
    }

    // Reativa as restrições de chave estrangeira
    await this.$executeRaw`SET foreign_key_checks = 1`;
  }

  async truncateTable(tablename) {
    if (tablename === undefined || tablename === '_prisma_migrations') {
      return;
    }
    try {
      await this.$executeRawUnsafe(`TRUNCATE ${tablename};`);
    } catch (error) {
      console.error({ error });
    }
  }
}
