import {
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';

@Injectable()
export default class BaseService<
  WhereUniqueInput,
  WhereInput,
  CreateInput,
  UpdateInput,
  Include,
> {
  protected _repository;
  logger = new Logger(BaseService.name);

  constructor(repository) {
    this._repository = repository;
  }

  async findOne(whereUniqueInput: WhereUniqueInput, include?: Include) {
    try {
      return await this._repository.findOne(whereUniqueInput, include);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(e.message);
    }
  }

  async findAll(params?: {
    skip?: number;
    take?: number;
    cursor?: any;
    where?: WhereInput;
    orderBy?: any;
    include?: Include;
    deleted?: boolean;
  }) {
    try {
      return await this._repository.findAll(params);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(e.message);
    }
  }

  async create(data: CreateInput) {
    try {
      return await this._repository.create(data);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(e.message);
    }
  }

  async update(where: WhereUniqueInput, data: UpdateInput) {
    try {
      return await this._repository.update({
        where: where,
        data,
      });
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(e.message);
    }
  }

  async delete(id: string, force: boolean) {
    try {
      await this._repository.delete({ id }, force);
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(e.message);
    }
  }
}
