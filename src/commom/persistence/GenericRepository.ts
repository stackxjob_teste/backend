import { ConflictException, Logger } from '@nestjs/common';

export default class BaseRepository<
  Domain,
  WhereUniqueInput,
  WhereInput,
  CreateInput,
  UpdateInput,
  Include,
> {
  protected _model;
  logger = new Logger(BaseRepository.name);
  constructor(model) {
    this._model = model;
  }

  async findOne(
    whereUniqueInput: WhereUniqueInput,
    include?: Include,
  ): Promise<Domain | null> {
    return this._model.findUnique({
      where: whereUniqueInput,
      include,
    });
  }

  async findUnique(
    whereInput: WhereInput,
    include?: Include,
  ): Promise<Domain | null> {
    return this._model.findUnique({
      where: whereInput,
      include,
    });
  }

  async findAll(params?: {
    skip?: number;
    take?: number;
    cursor?: any;
    where?: WhereInput;
    orderBy?: any;
    include?: Include;
    deleted?: boolean;
  }): Promise<Domain[]> {
    const { skip, take, cursor, where, orderBy, include, deleted } = params;

    let newWhere;

    if (deleted) {
      newWhere = {
        ...where,
      };
    } else {
      newWhere = {
        ...where,
        // NOT: {
        //   deleted_at: { not: null },
        // },
      };
    }

    return this._model.findMany({
      skip: skip ? +skip : undefined,
      take: take ? +take : undefined,
      cursor,
      where: newWhere,
      orderBy,
      include,
    });
  }

  async create(data: CreateInput): Promise<Domain> {
    try {
      return await this._model.create({
        data,
      });
    } catch (error) {
      this.logger.error(error);
      if (error.code === 'P2002') {
        throw new ConflictException();
      }
    }
  }

  async update(params: {
    where: WhereUniqueInput;
    data: UpdateInput;
  }): Promise<Domain> {
    const { data, where } = params;

    return this._model.update({
      data,
      where,
    });
  }

  async delete(where: WhereUniqueInput, force?: boolean): Promise<Domain> {
    if (force) {
      return this._model.delete({
        where,
      });
    }

    return this._model.update({
      data: {
        // deleted_at: new Date(),
      },
      where,
    });
  }
}
