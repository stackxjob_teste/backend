import { Module } from '@nestjs/common';
import SerialService from './serial.service';
import { PesosModule } from '../Pesos/pesos.module';
import PesosGateway from '../Pesos/pesos.gateway';
import { PrismaModule } from 'src/db/prisma.module';

@Module({
  imports: [PrismaModule, PesosModule],
  providers: [SerialService, PesosGateway],
  exports: [SerialService],
})
export class SerialModule {}
