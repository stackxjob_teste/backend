import { Injectable } from '@nestjs/common';
import { SerialPort } from 'serialport';
import PesosService from '../Pesos/pesos.service';
import PesosGateway from '../Pesos/pesos.gateway';
import PrismaService from 'src/db/prisma.service';

@Injectable()
export default class SerialService {
  private port: SerialPort;

  constructor(
    private readonly prisma: PrismaService,
    private readonly pesosService: PesosService,
    private readonly pesosGateway: PesosGateway,
  ) {
    this.port = new SerialPort({ path: 'COM1', baudRate: 9600 });

    this.port.on('data', (data) => {
      const weight = parseFloat(data.toString());

      if (weight) {
        this.saveWeight(weight);
      }
    });
  }

  async saveWeight(value: number) {
    const fornecedorIndex = Math.floor(Math.random() * 2);
    const fornecedor = await this.prisma.fornecedores.findMany();

    await this.pesosService.create({
      peso: value,
      supplier: {
        connect: {
          id: fornecedor[fornecedorIndex].id,
        },
      },
    });

    this.pesosGateway.sendNewPeso();
  }
}
