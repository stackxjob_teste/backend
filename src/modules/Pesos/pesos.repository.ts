import { Injectable } from '@nestjs/common';
import { Pesos, Prisma } from '@prisma/client';
import BaseRepository from 'src/commom/persistence/GenericRepository';
import PrismaService from 'src/db/prisma.service';

@Injectable()
export class PesosRepository extends BaseRepository<
  Pesos,
  Prisma.PesosWhereUniqueInput,
  Prisma.PesosWhereInput,
  Prisma.PesosCreateInput,
  Prisma.PesosUpdateInput,
  Prisma.PesosInclude
> {
  constructor(private prisma: PrismaService) {
    super(prisma.pesos);
  }

  async findMany() {
    return this.findAll({
      skip: 0,
      include: {
        supplier: {},
      },
    });
  }
}
