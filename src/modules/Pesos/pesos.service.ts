import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { PesosRepository } from './pesos.repository';
import { Prisma } from '@prisma/client';
import BaseService from 'src/commom/service/BaseService';

@Injectable()
export default class PesosService extends BaseService<
  Prisma.PesosWhereUniqueInput,
  Prisma.PesosWhereInput,
  Prisma.PesosCreateInput,
  Prisma.PesosUpdateInput,
  Prisma.PesosInclude
> {
  constructor(private repository: PesosRepository) {
    super(repository);
  }

  async findMany() {
    try {
      return await this.repository.findMany();
    } catch (e) {
      this.logger.error(e);
      throw new InternalServerErrorException(e.message);
    }
  }
}
