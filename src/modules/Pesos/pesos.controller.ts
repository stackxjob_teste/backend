import { Controller, Get, Logger } from '@nestjs/common';
import PesosService from './pesos.service';

@Controller('Pesos')
export class PesosController {
  private logger = new Logger(PesosController.name);
  constructor(private readonly pesosService: PesosService) {}

  @Get()
  async buscaPesos() {
    return this.pesosService.findMany();
  }
}
