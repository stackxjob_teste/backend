import { Module } from '@nestjs/common';
import { PesosController } from './pesos.controller';
import { PesosRepository } from './pesos.repository';
import { PrismaModule } from 'src/db/prisma.module';
import PesosService from './pesos.service';
import PesosGateway from './pesos.gateway';

@Module({
  imports: [PrismaModule],
  controllers: [PesosController],
  providers: [PesosService, PesosRepository, PesosGateway],
  exports: [PesosService, PesosGateway],
})
export class PesosModule {}
