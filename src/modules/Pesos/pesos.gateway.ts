import { WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server } from 'socket.io';

@WebSocketGateway({ cors: true })
export default class PesosGateway {
  @WebSocketServer()
  server: Server;

  sendNewPeso() {
    this.server.emit('newPeso');
  }
}
